require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get hom" do

    get root_path
    assert_response :success
  end

  test "should get aboout" do
    get about_path
    assert_response :success
  end

  test "should get help" do

    get help_path
    assert_response :success
  end

end
