class StaticPagesController < ApplicationController
  def hom 
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def aboout
  end

  def help
  end
end
